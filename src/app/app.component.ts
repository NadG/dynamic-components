import {Component, OnInit} from '@angular/core';
import { OverlayContainer } from '@angular/cdk/overlay';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
	themeClass: string;

	ngOnInit() {
		localStorage.clear();
		sessionStorage.clear();
	}
}
