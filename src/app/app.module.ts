// MODULE  AND ANGULAR IMPORTS
import {NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ReactiveFormsModule} from '@angular/forms';
import {FormsModule} from '@angular/forms';
import {AppComponent} from './app.component';
import {PtDynamicFormModule} from './commons/pt-dynamic-form/pt-dynamic-form.module';

// ANGULAR MATERIAL
import {
	MatButtonModule,
	MatCheckboxModule,
	MatPaginatorModule,
	MatRadioModule,
	MatSelectModule,
	MatSortModule
} from '@angular/material';
import {MatInputModule} from '@angular/material';
import {CdkTableModule} from '@angular/cdk/table';
import {MatTableModule} from '@angular/material';
import {MatDividerModule} from '@angular/material/divider';
import {MatTabsModule} from '@angular/material/tabs';
import {MatIcon} from "@angular/material";
// CUSTOM COMPONENTS

import {HomePageComponent} from './home-page/home-page.component';
import {DashboardComponent} from './home-page/dashboard/dashboard.component';
import {PtTableComponent} from './commons/pt-table/pt-table.component';
import {DataTableService} from './shared/services/data-table.service';
import {HttpClientModule} from '@angular/common/http';
import {SharedComponentsModule} from "./shared/modules/shared-components/shared-components.module";
import {PtUploadComponent} from './commons/pt-upload/pt-upload.component';

@NgModule({
	declarations: [
		AppComponent,
		HomePageComponent,
		DashboardComponent,
		PtTableComponent,
		PtUploadComponent
	],
	imports: [
		BrowserModule,
		BrowserAnimationsModule,
		FormsModule,
		ReactiveFormsModule,
		HttpClientModule,
		MatCheckboxModule,
		MatInputModule,
		MatButtonModule,
		MatRadioModule,
		MatSelectModule,
		MatDividerModule,
		CdkTableModule,
		MatTableModule,
		MatTabsModule,
		MatPaginatorModule,
		MatSortModule,
		SharedComponentsModule,
		PtDynamicFormModule,
	],
	schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
	providers: [
		DataTableService,
	],
	bootstrap: [AppComponent]
})
export class AppModule {
}
