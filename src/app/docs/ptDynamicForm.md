CAMPI RICHIESTI
----------------
I campi richiesti sono specificati nel file *'commons/pt-dynamic-form/components/formInterface.ts'*.

Basandosi su un file di configurazione ricevuto dal server, i campi del form possono essere:

* checkbox
* input
* link
* radio
* select
* textarea

VALIDAZIONE
-------------------
La validazione di tali campi sarà fornita da delle regex pervenute dal JSON di configurazione del server.

AGGIUNTA DI NUOVI COMPONENTI DINAMICI
--------------------
Se si vogliono aggiungere altri componenti da caricare dinamicamente occorre:
* creare un nuovo componente con l'angular-cli in pt-dynamic-form/components
* aggiungere una nuova chiave rappresentante il componente nell'oggetto const components in components/dynamic-field.directive.ts
* assicurarsi che nel file ts del nuovo componente ci sia il riferimento al FormGroup e all'oggetto di configurazione
* assicurarsi che nel template del nuovo componente ci sia il riferimento [formGroup] e [formControlName]

RIUTILIZZARE IL COMPONENTE
---------------
Per riutilizzare l'intero form dinamico basta includere il PtDynamicFormModule in AppModule e assicurarsi di avere angular/material installato
