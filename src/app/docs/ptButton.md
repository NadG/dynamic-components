##BOTTONI

###ATTRIBUTI
Questi sono gli attributi custom del componente **pt-button** e possono essere sia configurati manualmente
nell'html del componente oppure inseriti automaticamente (in quest'ultimo caso è consigliato ma non necessario wrappare pt-button nel
componente pt-button-group):

* **type**:					*Opzionale*. submit | reset | button (default)\
							Submit: qualsiasi bottone al cui click vengono mandati dati al sever 
* **disabled**; 			*Opzionale*. booleano che a seconda di una condizione abiliti o disabiliti il componente
* **btnName**: 			*Opzionale*.nome del bottone
* **btnIcon**: 			*Opzionale*. Se non presente è possibile inserire una icona custom all'interno del bottone o non averla affatto. 
							Se presente, e quindi con valore a true
* **btnBackground**: 	*Opzionale*. Colore di sfondo del bottone. 
							Accetta sia valori esadecimali sia i nomi dei colori secondo gli standard dei browser (es #000000 oppure black).
* **btnTextColor**:		*Opzionale*. Imposta il colore del testo del bottone.
							Accetta sia valori esadecimali sia i nomi dei colori secondo gli standard dei browser (es #000000 oppure black).
* **btnBorder** :			*Opzionale*. Imposta il bordo del bottone.
							I valori da inserire devono rispecchiare questo pattern: numero**px** solid | dashed | dotted *colore* (es: 1px solid red);
							Accetta sia valori esadecimali sia i nomi dei colori secondo gli standard dei browser (es #000000 oppure black).
---------------------------------------------------------------------------------------------