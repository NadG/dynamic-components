
import {CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PtButtonGroupComponent} from "../../../commons/pt-button-group/pt-button-group.component";
import {PtButtonComponent} from "../../../commons/pt-button/pt-button.component";
import {MatButtonModule,} from '@angular/material';
import {MatIcon} from '@angular/material';

@NgModule({
    imports: [
        CommonModule,
        MatButtonModule,
    ],
    declarations: [
        PtButtonGroupComponent,
        PtButtonComponent,
        MatIcon
    ],
    schemas: [ CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
    exports: [
        PtButtonGroupComponent,
        PtButtonComponent,
    ]
})
export class SharedComponentsModule {
}
