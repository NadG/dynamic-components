import {FormInterface} from '../../commons/pt-dynamic-form/components/formInterface';
import {ButtonInterface} from '../../commons/pt-button/pt-button';
import {TableSchemaInterface} from "../../commons/pt-table/table-inteface";

// PT FORM
export const ptFormSchema = {
	fields: [
		{
			type: 'input',
			name: 'nome',
			label: 'Nome Cognome',
			placeholder: 'Nome e cognome...',
			key: 'nome',
			value: '',
			validations: [
				{
					name: 'required',
					message: 'Nome Richiesto'
				},
				{
					name: 'minLength',
					minLength: 5,
					message: 'Inserire minimo 5 lettere'
				},
				{
					name: 'pattern',
					pattern: '^[A-Za-z.\\s_-]+$',
					message: 'Inserire solo caratteri alfabetici'
				}
			]
		},
		{
			type: 'input',
			name: 'luogo_nascita',
			label: 'Luogo di nascita',
			placeholder: 'Luogo di nascita..',
			key: 'luogo_nascita',
			value: '',
			validations: [
				{
					name: 'required',
					message: 'Nome Richiesto'
				},
				{
					name: 'pattern',
					pattern: '^[A-Za-z.\\s_-]+$',
					message: 'Inserire solo caratteri alfabetici'
				}
			]
		},
		{
			type: 'datepicker',
			name: 'data_nascita',
			label: 'Data di nascita',
			placeholder: 'Data di nascita',
			key: 'data_nascita',
			value: '',
			validations: [
				{
					name: 'required',
					message: 'Compilazione richiesta'
				},
			]
		},
		{
			type: 'radio',
			name: 'contratto',
			label: 'Contratto',
			options: [
				'Contratto Indeterminato',
				'Stage'
			],
			key: 'contratto',
			value: '',
			validations: [
				{
					name: 'required',
					message: 'Campo richiesto'
				}
			]
		},
		{
			type: 'input',
			name: 'email',
			label: 'Email',
			placeholder: 'Email',
			key: 'email',
			value: '',
			validations: [
				{
					name: 'required',
					message: 'Campo richiesto'
				},
				{
					name: 'pattern',
					pattern: '(?:[a-z0-9!#$%&\'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&\'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\\])',
					message: 'Inserire una mail valida'
				}
			]
		},
		{
			type: 'select',
			name: 'impiego',
			label: 'Impiego',
			options: [
				'Cto',
				'Senior Developer',
				'Junior Developer',
				'Project Manager'
			],
			placeholder: 'Seleziona...',
			key: 'impiego',
			// value: '',
			// validations: [
			// 	{
			// 		name: 'required',
			// 		message: 'Campo richiesto'
			// 	},
			// ]
		},
		{
			type: 'checkbox',
			name: 'checkbox',
			label: 'Skills',
			placeholder: 'checkbox',
			options: [
				{
					name: 'JS',
					checked: false
				},
				{
					name: 'CSS',
					checked: false
				},
				{
					name: 'PHP',
					checked: false
				},
			],
			key: 'checkbox'
		},
	],
	buttons: [
		{
			type: 'submit',
			btnMatType: 'mat-raised-button',
			btnName: 'Cancel',
			btnIcon: 'arrow_back',
			eventName: 'cancelEvent',
		},
		{
			type: 'submit',
			btnMatType: 'mat-raised-button',
			btnName: 'Submit',
			btnIcon: 'send',
			eventName: 'submitEvent',
			btnMatColor: 'primary'
		},
	]
};

// PT TABLE
export const dataSourceCtrl: Array<Object> = [
	{
		nome: 'John Coltrane',
		luogo_nascita: 'Milano',
		data_nascita: '13/09/1991',
		impiego: 'Junior Developer',
		options: ['JS', 'CSS'], //  chiave per indicare i valori pervenuti da una o più checkbox
		contratto: 'Stage',
		email: 'jc@mail.it'
	},
	{
		nome: 'Mag Colane',
		luogo_nascita: 'Londra',
		data_nascita: '15/09/1986',
		impiego: 'Senior Developer',
		contratto: 'Contratto Indeterminato',
		email: 'mc@mail.it'
	},
	{
		nome: 'Nemi Trane',
		luogo_nascita: 'Torino',
		data_nascita: '1/3/1971',
		impiego: 'Project Manager',
		contratto: 'Contratto Indeterminato',
		email: 'nt@mail.it'
	},
];

export const ptTableSchema: TableSchemaInterface = {
	displayedColumns: [
		'nome',
		'luogo_nascita',
		'data_nascita',
		'impiego',
		'options',
		'contratto',
		'email',
		'actions'
	],
	fields: [
		{key: 'nome', label: 'Nome'},
		{key: 'luogo_nascita', label: 'Luogo di nascita'},
		{key: 'data_nascita', label: 'Data di nascita'},
		{key: 'impiego', label: 'Impiego'},
		{key: 'options', label: 'Skills'},
		{key: 'contratto', label: 'Contratto'},
		{key: 'email', label: 'Email'},
		{key: 'actions', label: 'Azioni'},
	],
	buttons: [
		{type: 'submit', btnIcon: 'create', eventName: 'editEvent', btnMatColor: 'primary'},
		{type: 'submit', btnIcon: 'delete', eventName: 'deleteEvent', btnMatColor: 'primary'},
	]
};

export const actionBarSchema: ButtonInterface = [
	{
		type: 'submit',
		btnIcon: 'add',
		btnMatColor: 'primary',
		eventName: 'addEvent',
		btnName: 'Add row',
		btnMatType: 'mat-raised-button'
	},
]

// PT UPLOAD

export const dataSourceCtrl3: Array<Object> = [
];

export const ptTableSchema3: TableSchemaInterface = {
	displayedColumns: [
		'icon',
		'image',
		'name',
		'dimensions',
		'actions'
	],
	fields: [
		{key: 'icon', label: 'Icona'},
		{key: 'image', label: 'Anteprima'},
		{key: 'name', label: 'Nome file'},
		{key: 'dimensions', label: 'Dimensioni'},
		{key: 'actions', label: 'Azioni'},
	],
	buttons: [
		{
			type: 'submit',
			btnIcon: 'file_upload',
			btnMatColor: 'primary',
			eventName: 'uploadEvent',
			disabled: true
		},
		{
			type: 'submit',
			btnIcon: 'cancel',
			btnMatColor: 'primary',
			eventName: 'cancelEvent',
			disabled: true
		},
		{
			type: 'submit',
			btnIcon: 'delete',
			btnMatColor: 'primary',
			eventName: 'deleteEvent',
		},
	]
};

export const uploadActionBarSchema = {
	buttons: [
		{
			type: 'file',
			btnIcon: 'file_upload',
			accept: ".png,.jpg,.jpeg",
			btnMatColor: 'primary',
			eventName: 'uploadEvent',
			btnName: 'Upload',
			disabled: false,
			btnMatType: 'mat-raised-button'
		}
	]
}

// examples per la home
export const exampleBtnGroup: ButtonInterface = [
	{
		type: 'submit',
		btnIcon: 'brush',
		btnMatColor: 'primary',
		btnName: 'Raised',
		btnMatType: 'mat-raised-button'
	},
	{
		type: 'submit',
		btnMatColor: 'primary',
		btnName: 'No icon',
		btnMatType: 'mat-raised-button'
	},
	{
		type: 'submit',
		btnMatColor: 'primary',
		btnName: 'Disabled',
		btnMatType: 'mat-raised-button',
		disabled: 'true'
	},
	{
		type: 'submit',
		btnIcon: 'brush',
		btnMatColor: 'primary',
		btnName: 'Stroked',
		btnMatType: 'mat-stroked-button'
	},
	{
		type: 'submit',
		btnIcon: 'brush',
		btnMatColor: 'primary',
		btnName: 'Flat',
		btnMatType: 'mat-flat-button'
	},
	{
		type: 'submit',
		btnIcon: 'brush',
		btnMatColor: 'primary',
		btnMatType: 'mat-fab-button'
	},
	{
		type: 'submit',
		btnIcon: 'brush',
		btnMatColor: 'primary',
		eventName: 'testEvent',
		btnMatType: 'mat-mini-fab'
	}
]

export const dataSourceCtrl2: Array<Object> = [
	{nome: 'John Coltrane', data_nascita: '13/09/1991', impiego: 'Developer'},
	{nome: 'Mag Colane', data_nascita: '15/09/1986', impiego: 'Cto'},
	{nome: 'Nemi Trane', data_nascita: '1/3/1971', impiego: 'Ceo'},
];

export const ptTableSchema2: TableSchemaInterface = {
	displayedColumns: [
		'nome',
		'data_nascita',
		'impiego',
		'actions'
	],
	fields: [
		{key: 'nome', label: 'Nome'},
		{key: 'data_nascita', label: 'Data di nascita'},
		{key: 'impiego', label: 'Impiego'},
		{key: 'actions', label: 'Azioni'},
	],
	buttons: [
		{type: 'submit', btnIcon: 'delete', eventName: 'deleteEvent', btnMatColor: 'primary'},
	]
};

export const ptFormSchema2: FormInterface = {
	fields: [
		{
			type: 'input',
			name: 'nome',
			label: 'Semplice input',
			placeholder: 'Semplice input',
			key: 'input',
			value: '',
			validations: [
				{
					name: 'required',
					message: 'Campo richiesto'
				},
				{
					name: 'minLength',
					minLength: 5,
					message: 'Inserire minimo 5 lettere'
				},
				{
					name: 'pattern',
					pattern: '^[A-Za-z.\\s_-]+$',
					message: 'Inserire solo caratteri alfabetici'
				}
			]
		},
		{
			type: 'datepicker',
			name: 'data_nascita',
			label: 'Data di nascita',
			placeholder: 'Datapicker',
			key: 'datapicker',
			value: '17/10/1980',
		},
		{
			type: 'radio',
			name: 'radio',
			label: 'radio',
			options: [
				'Valore 1',
				'Valore 2',
				'Valore 3'
			],
			key: 'radio',
			value: 'Valore 2',
		},
		{
			type: 'select',
			name: 'select',
			label: 'Select',
			options: [
				'A',
				'B',
				'C',
				'D',
			],
			placeholder: 'Seleziona...',
			key: 'impiego',
			value: 'C',
			validations: [
				{
					name: 'required',
					message: 'Campo richiesto'
				}
			]
		},
		{
			type: 'textarea',
			name: 'textarea',
			label: 'Textarea',
			placeholder: 'Textarea',
			key: 'biografia',
			value: 'Ninja',
		},
		{
			type: 'slider',
			name: 'slider',
			label: 'Slider',
			placeholder: 'Slider',
			key: 'range',
			value: '75',
		},
		{
			type: 'toggle',
			name: 'toggle',
			label: 'Toggle',
			placeholder: 'toggle',
			key: 'toggle'
		},
		{
			type: 'checkbox',
			name: 'checkbox',
			label: 'Checkbox',
			placeholder: 'checkbox',
			options: [
				'Valore 1',
				'Valore 2',
				'Valore 3'
			],
			key: 'checkbox',
			validations: [
				{
					name: 'required',
					message: 'Campo richiesto'
				}
			]
		},

	],
	buttons: [
		{
			type: 'submit',
			btnMatType: 'mat-raised-button',
			btnName: 'Cancel',
			btnIcon: 'arrow_back',
			eventName: 'clearEvent',
		},
		{
			type: 'submit',
			btnMatType: 'mat-raised-button',
			btnName: 'Submit',
			btnIcon: 'send',
			eventName: 'submitEvent',
			btnMatColor: 'primary'
		},
	]
};
