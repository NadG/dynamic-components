
import {of as observableOf, Observable} from 'rxjs';

import {delay} from 'rxjs/operators';
import {Injectable} from '@angular/core';
import {ptFormSchema, ptFormSchema2} from "../mocks/mock-resource-manager";
import {FormInterface} from "../../commons/pt-dynamic-form/components/formInterface";

@Injectable({
	providedIn: 'root'
})
export class FormConfigService {

	constructor() {
	}

	getFormConfig(): Observable<any> {
	// getFormConfig(): Observable<FormInterface> {
		return observableOf(ptFormSchema, ptFormSchema2).pipe(delay(0));
	}
}
