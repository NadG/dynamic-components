import {of as observableOf, Observable} from 'rxjs';
import {Injectable} from '@angular/core';
import {actionBarSchema, exampleBtnGroup, uploadActionBarSchema} from "../mocks/mock-resource-manager";
import {ButtonInterface} from "../../commons/pt-button/pt-button";

@Injectable({
	providedIn: 'root'
})
export class ButtonsUtilitiesService {

    constructor() {
    }

	getButtonsConfig(): Observable<any>{
		return observableOf(actionBarSchema,  exampleBtnGroup, uploadActionBarSchema);
	}
}
