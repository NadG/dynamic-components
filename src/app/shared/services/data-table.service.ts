import {of as observableOf, Observable, Subject} from 'rxjs';
import {delay} from 'rxjs/operators';
import {Injectable} from '@angular/core';
import {
	dataSourceCtrl,
	ptTableSchema,
	dataSourceCtrl2,
	ptTableSchema2,
	dataSourceCtrl3, ptTableSchema3
} from "../mocks/mock-resource-manager";
import 'rxjs/Rx';
import {TableSchemaInterface} from "../../commons/pt-table/table-inteface";

@Injectable({
	providedIn: 'root'
})
export class DataTableService {

	constructor() {
	}

	getDataSourceConfig(): Observable<Array<Object>> {
		return observableOf(dataSourceCtrl, dataSourceCtrl2, dataSourceCtrl3).pipe(delay(0));
	}

    getTableSchemaConfig(): Observable<TableSchemaInterface> {
        return observableOf(ptTableSchema, ptTableSchema2, ptTableSchema3).pipe(delay(0));
    }
}
