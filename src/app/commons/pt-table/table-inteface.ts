import {ButtonInterface} from "../pt-button/pt-button";

export interface TableSchemaInterface  {
    displayedColumns?: Array<String>;
    fields: Array<Object>;
    buttons: ButtonInterface
}
