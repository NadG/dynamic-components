import {
	ChangeDetectorRef,
	Component,
	Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild
} from '@angular/core';
import {MatTable, MatTableDataSource} from '@angular/material';

@Component({
	selector: 'pt-table',
	templateUrl: './pt-table.component.html',
	styleUrls: ['./pt-table.component.scss'],

})
export class PtTableComponent implements  OnInit {
	@Input() tableSchema: any;
	@Input() dataSource;

	constructor() {
	}

	ngOnInit() {

	}
}
