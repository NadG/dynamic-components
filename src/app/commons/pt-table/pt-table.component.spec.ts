import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PtTableComponent } from './pt-table.component';

describe('PtTableComponent', () => {
  let component: PtTableComponent;
  let fixture: ComponentFixture<PtTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PtTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PtTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
