import { NgModule ,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import {CommonModule} from '@angular/common';
import {ReactiveFormsModule} from '@angular/forms';
import { DynamicFieldDirective } from './components/dynamic-field.directive';
import { FormInputComponent } from './components/form-input/form-input.component';
import { FormSelectComponent } from './components/form-select/form-select.component';
import { DynamicFormComponent } from './container/dynamic-form/dynamic-form.component';
import { FormRadioComponent } from './components/form-radio/form-radio.component';
import { FormTextareaComponent } from './components/form-textarea/form-textarea.component';
import { FormCheckboxComponent } from './components/form-checkbox/form-checkbox.component';

import {FormDatepickerComponent} from './components/form-datepicker/form-datepicker.component';
import {SharedComponentsModule} from "../../shared/modules/shared-components/shared-components.module";

@NgModule({
	imports: [
		CommonModule,
		ReactiveFormsModule,
        SharedComponentsModule
	],
	declarations: [
		DynamicFieldDirective,
		FormInputComponent,
		FormSelectComponent,
		DynamicFormComponent,
		FormRadioComponent,
		FormTextareaComponent,
		FormCheckboxComponent,
		FormDatepickerComponent
	],
	entryComponents: [
		FormInputComponent,
		FormSelectComponent,
		FormRadioComponent,
		FormTextareaComponent,
		FormCheckboxComponent,
		FormDatepickerComponent
	],
	exports: [
		DynamicFormComponent,
	],
	schemas: [
        CUSTOM_ELEMENTS_SCHEMA,
    ]
})
export class PtDynamicFormModule {
}
