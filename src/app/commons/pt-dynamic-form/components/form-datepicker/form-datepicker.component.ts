import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';
import moment from 'moment';

@Component({
	selector: 'pt-form-datepicker',
	templateUrl: './form-datepicker.component.html',
	styleUrls: ['./form-datepicker.component.scss'],
})
export class FormDatepickerComponent implements OnInit, OnDestroy  {
	config;
	group: FormGroup;
	currentDateVal;

	ngOnInit() {
		console.log('this.constructor.name' + this.constructor.name);
		this.currentDateVal = this.group.get(this.config.name).value ;
		let p = this.currentDateVal.toString().split('/')
		let d = p[0];
		let m = p[1];
		let y = p[2];
		this.group.get(this.config.name).setValue( y + '-' + m + '-' + d)
	}

	setNewDate() {
		console.log(  this.group.get(this.config.name).value);
		this.currentDateVal = moment(this.group.get(this.config.name).value).format('YYYY-MM-DD') ;

		console.log( this.currentDateVal );
		// let newDate = moment( this.currentDateVal).format( 'DD/MM/YYYY');
		// console.log( newDate );
		// this.group.get(this.config.name).patchValue(newDate);
		this.group.get(this.config.name).patchValue(this.currentDateVal);
	}

	ngOnDestroy() {
		let newDate = moment( this.currentDateVal).format( 'DD/MM/YYYY');
		console.log( newDate );
		this.group.get(this.config.name).patchValue(newDate);
	}
}
