import {Component, OnInit} from '@angular/core';
import {FormArray, FormControl, FormGroup} from '@angular/forms';
import {MAT_CHECKBOX_CLICK_ACTION} from "@angular/material";

@Component({
	selector: 'pt-form-checkbox',
	templateUrl: './form-checkbox.component.html',
	styleUrls: ['./form-checkbox.component.scss'],
	providers: [
		{provide: MAT_CHECKBOX_CLICK_ACTION, useValue: 'check'}
	]
})
export class FormCheckboxComponent implements OnInit {
	config;
	group: FormGroup;
	configOptions = []
	existentOptions
	controlName: any
	checked = []

	constructor() {
	}

	ngOnInit() {
		console.log('this.constructor.name ' + this.constructor.name);
		this.controlName = this.config.name;
		 this.configOptions = this.config.options;
		 console.log( this.config );
		// // controllo se esistono dei valori checkati
		// this.group.value.options ? this.existentOptions = this.group.value.options : null
		// console.log( this.existentOptions );

	}

	onChange(data) {
		console.log( data );
		console.log( this.group );
		let control = <FormArray>this.group.get('options') as FormArray;
		console.log(  control);
		if (data ) {
			control.push(new FormControl(data))
			console.log( control );
		}
		// else {
		// 	const i = control.controls.findIndex(x => x.value === event.source.value);
		// 	control.removeAt(i);
		// }
	}

}
