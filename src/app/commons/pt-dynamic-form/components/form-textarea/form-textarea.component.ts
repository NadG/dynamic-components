import {Component, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';

@Component({
	selector: 'pt-form-textarea',
	templateUrl: './form-textarea.component.html',
	styleUrls: ['./form-textarea.component.scss']
})
export class FormTextareaComponent implements OnInit {
	config;
	group: FormGroup;

	constructor() {
	}

	ngOnInit() {
		console.log('this.constructor.name ' + this.constructor.name);
	}

}
