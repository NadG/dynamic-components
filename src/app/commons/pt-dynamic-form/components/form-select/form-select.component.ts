import {Component, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';

@Component({
	selector: 'pt-form-select',
	templateUrl: './form-select.component.html',
	styleUrls: ['./form-select.component.scss']
})
export class FormSelectComponent implements OnInit {
	config;
	group: FormGroup;
	default;

	constructor() {}

	ngOnInit() {
		console.log('this.constructor.name ' + this.constructor.name);
		// set default value
		let c = this.group.get(this.config.name).value;

		// if (typeof c === 'string') {
		// 	this.default = this.group.get(this.config.name).value
		// } else {
		// 	this.default = this.group.get(this.config.name).value[0]
		// }
	}
}
