import {Component, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {animate, style, transition, trigger} from '@angular/animations';
import { ViewEncapsulation } from '@angular/core';

@Component({
	selector: 'pt-form-input',
	templateUrl: './form-input.component.html',
	styleUrls: ['./form-input.component.scss'],
	animations: [
		trigger(
			'error',
			[
				transition(
					':enter', [
						style({transition: 'ease-in', opacity: 0}),
						animate('50ms', style({transition: 'ease-in', 'opacity': 1}))
					]
				),
				transition(
					':leave', [
						style({transition: 'ease-out', opacity: 1}),
						animate('1s', style({transition: 'ease-out', 'opacity': 0}))

					]
				)]
		)
	],
	encapsulation: ViewEncapsulation.None
})
export class FormInputComponent implements OnInit {
	config;
	group: FormGroup;

	constructor() {
	}

	ngOnInit() {
		console.log('this.constructor.name ' + this.constructor.name);
	}


}
