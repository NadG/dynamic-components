import {Component, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {MatRadioChange} from "@angular/material";

@Component({
	selector: 'pt-form-radio',
	templateUrl: './form-radio.component.html',
	styleUrls: ['./form-radio.component.scss']
})
export class FormRadioComponent implements OnInit {
	config;
	group: FormGroup;
	checked: boolean;
	constructor() {
	}

	ngOnInit() {
		console.log('this.constructor.name ' + this.constructor.name);
		console.log( this.config );
	}

    radioChange(event: MatRadioChange) {
        this.group.value.gender = event.value;
    }
}
