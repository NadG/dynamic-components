import {ButtonInterface} from "../../pt-button/pt-button";

export interface ValidatorInterface {
	name: string;
	message?: string;
    pattern?: string;
    minLength?: number,
    maxLength?: number,
    maxValue?: number,
    minValue?: number,
}
export interface FieldInterface {
    type: string;
    name: string;
    key: string;
    value?: any;
    checked?: boolean;
    label?: string;
    placeholder?: string;
    options?: any;
    href?: string;
    target?: string;
    validations?: ValidatorInterface[];
}
export interface FormInterface {
    fields: FieldInterface[];
    buttons: ButtonInterface;
}
