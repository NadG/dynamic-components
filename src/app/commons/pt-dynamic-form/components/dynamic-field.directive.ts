import {ComponentFactoryResolver, Directive, Input, OnInit, ViewContainerRef} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {FormInputComponent} from './form-input/form-input.component';
import {FormSelectComponent} from './form-select/form-select.component';
import {FormRadioComponent} from './form-radio/form-radio.component';
import {FormTextareaComponent} from './form-textarea/form-textarea.component';
import {FormCheckboxComponent} from './form-checkbox/form-checkbox.component';
import {FormDatepickerComponent} from './form-datepicker/form-datepicker.component';

const components = {
	input: FormInputComponent,
	select: FormSelectComponent,
	radio: FormRadioComponent,
	textarea: FormTextareaComponent,
	checkbox: FormCheckboxComponent,
	datepicker: FormDatepickerComponent
};

@Directive({
	selector: '[ptDynamicField]'
})
export class DynamicFieldDirective  implements OnInit{
	@Input() config;
	@Input() group: FormGroup;

	constructor(
		private resolver: ComponentFactoryResolver,
		private container: ViewContainerRef
	) {}

	ngOnInit() {
		let component = components[this.config.type];
		let factory = this.resolver.resolveComponentFactory<any>(component);
		component = this.container.createComponent(factory);
		component.instance.config = this.config;
		component.instance.group = this.group;
	}
}
