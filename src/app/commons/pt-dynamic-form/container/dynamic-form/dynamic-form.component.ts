import {Component, Input, OnInit, AfterViewChecked, ChangeDetectorRef} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
	selector: 'pt-dynamic-form',
	templateUrl: './dynamic-form.component.html',
	styleUrls: ['./dynamic-form.component.scss'],

})
export class DynamicFormComponent implements OnInit {

	form: FormGroup;
	@Input() existentData;
	@Input() config;
	@Input() validList;
	public isValidMsg: string;
	existentOptions = [];
	exOp;
	constructor(
		private fb: FormBuilder,
		private cdRef: ChangeDetectorRef
	) {
	}

	ngOnInit() {
		console.log('this.constructor.name ' + this.constructor.name);
		setTimeout(() => {
			this.form = this.createGroup();
		}, 50);
	}

	// ngAfterViewChecked() {
	// 	this.cdRef.detectChanges();
	// }

	createGroup() {
		const group = this.fb.group({
			options: this.fb.array( [])
		});

		this.config.fields.forEach(config => {
			group.addControl(config.name,
				this.fb.control(this.existentData ? group.patchValue(this.existentData) : config.value,
					this.bindValidations(config.validations || [])
				)
			)}
		);
		console.log( group );
		return group;
	}

	bindValidations(validations: any) {
		if (validations.length > 0) {
			const validList = [];
			validations.forEach(v => {
				switch (v.name) {
					case 'required':
						return validList.push(Validators.required);
					case 'min':
						return validList.push(Validators.min(v.min));
					case 'max':
						return validList.push(Validators.max(v.minLength));
					case 'email':
						return validList.push(Validators.email);
					case 'pattern':
						return validList.push(Validators.pattern(v.pattern));
					case 'minLength':
						return validList.push(Validators.minLength(v.minLength));
					case 'maxLength':
						return validList.push(Validators.maxLength(10));
					case undefined:
						return null
				}
				console.log(validList);
			});
			return Validators.compose(validList);
		}
		return null;
	}
}
