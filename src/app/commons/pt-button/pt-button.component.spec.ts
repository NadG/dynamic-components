import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {PtButtonComponent} from './pt-button.component';

describe('PtButtonComponent', () => {
	let component: PtButtonComponent;
	let fixture: ComponentFixture<PtButtonComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [PtButtonComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(PtButtonComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
