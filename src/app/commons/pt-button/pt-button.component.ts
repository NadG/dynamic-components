import {Component, Renderer2, Input, OnInit, Output, EventEmitter, ElementRef} from '@angular/core';
import {ButtonInterface} from './pt-button';

@Component({
	selector: 'pt-button',
	templateUrl: './pt-button.component.html',
	styleUrls: ['./pt-button.component.scss']
})
export class PtButtonComponent implements OnInit {
	@Input() item?: Object;
	@Input() index: Number;
	@Input() dataSource?: any;
	@Input() type?: ButtonInterface[];
	@Input() btnMatType?: any;
	@Input() btnMatColor?: ButtonInterface[];
	@Input() disabled?: ButtonInterface[];
	@Input() btnName?: ButtonInterface[];
	@Input() btnIcon?: ButtonInterface[];
	@Input() btnBackground?: ButtonInterface[];
	@Input() btnBorder?: ButtonInterface[];
	@Input() btnTextColor?: ButtonInterface[];
	@Output() btnData = new EventEmitter<any>();

	constructor(private el: ElementRef, private renderer: Renderer2) {}

	ngOnInit() {
		console.log('this.constructor.name ' + this.constructor.name);
		this.setRemoteStyles();
	}

	btnEvent() {
		console.log( this.item )
		this.btnData.emit({
            index: this.index,
			item: this.item
		});
	}

	// da rivedere
	setRemoteStyles() {
		this.renderer.setStyle(this.el.nativeElement, 'background-color', this.btnBackground);
		this.renderer.setStyle(this.el.nativeElement, 'color', this.btnTextColor);
		this.renderer.setStyle(this.el.nativeElement, 'border', this.btnBorder);
	}

}

