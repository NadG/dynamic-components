export interface ButtonInterface extends Array<Object> {
	type?: string;
	btnIcon?: string;
	accept?: string;
	btnMatColor?: string;
	btnName?: string;
	btnMatType?: string;
	disabled?: boolean;
	btnBorder?: string;
	btnTextColor?: string;
	btnBackground?: string;
	eventName?: string;
}
