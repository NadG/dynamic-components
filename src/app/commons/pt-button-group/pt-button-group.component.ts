import {Component, ElementRef, Input, OnInit, } from '@angular/core';
import 'rxjs/Rx';

@Component({
	selector: 'pt-button-group',
	templateUrl: './pt-button-group.component.html',
	styleUrls: ['./pt-button-group.component.scss']
})
export class PtButtonGroupComponent implements OnInit {
    @Input() items: Array<any>;
	@Input() item;

    constructor(private el: ElementRef) {
    }

    ngOnInit() {
        console.log('this.constructor.name ' + this.constructor.name);
    }

    onParentClick(data) {
        console.log( data.item )
        this.el.nativeElement.dispatchEvent(new CustomEvent( this.items[data.index].eventName , {
        	detail: data.item,
        	bubbles: true,
        	cancelable: true
        }));
    }
}
