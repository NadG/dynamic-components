import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PtButtonGroupComponent } from './pt-button-group.component';

describe('PtButtonGroupComponent', () => {
  let component: PtButtonGroupComponent;
  let fixture: ComponentFixture<PtButtonGroupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PtButtonGroupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PtButtonGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
