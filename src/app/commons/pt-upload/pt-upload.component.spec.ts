import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PtUploadComponent } from './pt-upload.component';

describe('PtUploadComponent', () => {
  let component: PtUploadComponent;
  let fixture: ComponentFixture<PtUploadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PtUploadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PtUploadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
