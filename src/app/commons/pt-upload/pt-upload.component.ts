import {Component, Input, OnInit} from '@angular/core';
import {FormControl} from "@angular/forms";

@Component({
	selector: 'pt-upload',
	templateUrl: './pt-upload.component.html',
	styleUrls: ['./pt-upload.component.scss']
})
export class PtUploadComponent implements OnInit {
	@Input() config;
	@Input() schema
	@Input() dataSource
	upload = new FormControl('');
	size: number = 0;
	unit: string = "";
	fileExtention: string;
	fileName: string;
	sizeUnit: string = `${this.size}+${this.unit}`;
	icon: any;
	previewImg;
	previewMultipleImg = [];
	fileList = [];

	constructor() {}

	ngOnInit() {}

	build(event) {
		this.formatFileName();
		this.getExtention(this.fileName)
		this.getSize(event)
		this.previewFile(event)
	}

	formatFileName() {
		console.log(this.upload.value);
		let filename = this.upload.value.replace(/^.*\\/, "");
		return this.fileName = filename;
	}

	getExtention(filename) {
		this.fileExtention = filename.split(/\.(?=[^\.]+$)/).slice(-1).pop().toString();
		return this.fileExtention
	}

	getSize(event) {
		const size = event.srcElement.files[0].size;
		if (size < 1000) {
			this.size = size;
			this.unit = "bytes";
		} else if (size < 1000 * 1000) {
			this.size = size / 1000;
			this.unit = "kb";
		} else if (size < 1000 * 1000 * 1000) {
			this.size = size / 1000 / 1000;
			this.unit = "mb";
		} else {
			this.size = size / 1000 / 1000 / 1000;
			this.unit = "gb";
		}
		return this.sizeUnit = `${this.size}${this.unit}`
	}

	previewFile(event) {
		let files = event.target.files;
		if (files.length === 1) {
			let reader = new FileReader();
			reader.readAsDataURL(files[0]);
			reader.onload = () => {
				return this.previewImg = reader.result
			}
		}
		else if (files.length > 1) {
			Object.values(files).map(e => {
				let reader = new FileReader();
				// @ts-ignore
				reader.readAsDataURL(e)
				 reader.onload = () => {
					let log = reader.result
					 this.previewMultipleImg.push(log)
					return this.previewMultipleImg
				}
				return this.previewMultipleImg
			})
		}
	}
}
