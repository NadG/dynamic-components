import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {DataTableService} from '../../shared/services/data-table.service';
import {elementAt} from 'rxjs/operators';
import {FormConfigService} from '../../shared/services/form-config.service';
import {ButtonsUtilitiesService} from "../../shared/services/buttons-utilities.service";
import {Subscription} from "rxjs";
import {MatTable, MatTableDataSource} from "@angular/material";
import {PtTableComponent} from "../../commons/pt-table/pt-table.component";
import {PtUploadComponent} from "../../commons/pt-upload/pt-upload.component";

@Component({
	selector: 'app-dashboard',
	templateUrl: './dashboard.component.html',
	styleUrls: ['./dashboard.component.scss'],
})

export class DashboardComponent implements OnInit, OnDestroy, AfterViewInit {
	public uploadConfig = {}
	public dataSourceCtrl;
	public dataSourceCtrl3;
	public ptTableSchema;
	public ptTableSchema3;
	public configForm;
	public dataSourceEdited;
	public dataSourceEdited2;
	public actionBarConfig;
	public actionBar2Config
	public rowSelected;
	public showForm = false;
	private _fromAction;
	fileKey;
	private subscription: Subscription = new Subscription();

	@ViewChild(PtTableComponent) table;
	@ViewChild(PtUploadComponent) upload;

	constructor(
		private _dts: DataTableService,
		private _fcs: FormConfigService,
		private _bus: ButtonsUtilitiesService
	) {
	}

	ngOnInit() {
		console.log('this.constructor.name' + this.constructor.name);
		this.configOnInit();
	}

	ngAfterViewInit() {
		console.log(this.table._dataSource);
	}

	configOnInit() {

		// PT-BUTTON-GROUP : ACTIONBAR
		this.subscription.add(this._bus.getButtonsConfig()
			.pipe(elementAt(0))
			.subscribe((res: any) => {
				this.actionBarConfig = res;
				return this.actionBarConfig;
			})
		)

		this.subscription.add(this._bus.getButtonsConfig()
			.pipe(elementAt(2))
			.subscribe((res: any) => {
				this.actionBar2Config = res;
				Object.assign(this.uploadConfig, this.actionBar2Config)
				console.log(this.uploadConfig);
				return this.uploadConfig
			})
		)
		// PT TABLE
		this.subscription.add(this._dts.getDataSourceConfig()
			.pipe(elementAt(0))
			.subscribe((res: any) => {
				this.dataSourceCtrl = res;
				return this.dataSourceCtrl;
			})
		)

		this.subscription.add(this._dts.getDataSourceConfig()
			.pipe(elementAt(2))
			.subscribe((res: any) => {
				this.dataSourceCtrl3 = res;
				Object.assign(this.uploadConfig, {dataSource: this.dataSourceCtrl3})
				return this.uploadConfig;
			})
		)

		this.subscription.add(this._dts.getTableSchemaConfig()
			.pipe(elementAt(0))
			.subscribe((res: any) => {
				this.ptTableSchema = res;
				return this.ptTableSchema;
			})
		)

		this.subscription.add(this._dts.getTableSchemaConfig()
			.pipe(elementAt(2))
			.subscribe((res: any) => {
				this.ptTableSchema3 = res;
				Object.assign(this.uploadConfig, {tableSchema: this.ptTableSchema3})
				return this.uploadConfig;
			})
		)

		// PT FORM
		this.subscription.add(this._fcs.getFormConfig()
			.pipe(elementAt(0))
			.subscribe((res: any) => {
				this.configForm = res;
				return this.configForm;
			})
		);
	}

	uploadFiles($event) {
		let img = $event.detail.image
		localStorage.setItem($event.detail.name, img);
		this.fileKey = $event.detail.name;
		this.dataSourceCtrl3.push($event.detail)
	}

	deleteRow($event) {
		this.rowSelected = $event.detail;
		this.dataSourceCtrl.splice(
			this.rowSelected.rowIndex, 1
		);
	}

	deleteFile($event) {
		this.rowSelected = $event.detail;
		this.dataSourceCtrl3.splice(
			this.rowSelected.rowIndex, 1
		);
		localStorage.removeItem(this.fileKey)
	}

	addRow() {
		this.showForm = true;
		this.rowSelected = '';
		this._fromAction = 'addRow';
	}

	editRow($event) {
		this.showForm = true;
		this.rowSelected = $event.detail;
		this._fromAction = 'editRow';
	}

	onSubmit($event) {
		switch (this._fromAction) {
			case 'addRow' : {
				this.dataSourceCtrl.push($event.detail);
				console.log($event.detail);
			}
				break;
			case 'editRow' : {
				if (this.rowSelected.row !== $event.detail) {
					this.dataSourceCtrl.splice(
						this.rowSelected.rowIndex, 1, $event.detail
					);
				}
			}
		}
		this.showForm = $event.showForm;
		this._fromAction = '';
	}

	onCancel() {
		this.showForm = false;
	}

	ngOnDestroy() {
		this.subscription.unsubscribe();
		if (!this.subscription.closed) {
			console.log('NOT SUBSCRIBED!');
		} else {
			console.log('Unsubscripted');
		}
	}
}
