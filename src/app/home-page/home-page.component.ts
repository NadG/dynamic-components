import {Component, OnDestroy, OnInit} from '@angular/core';
import {elementAt} from "rxjs/operators";
import {ButtonsUtilitiesService} from "../shared/services/buttons-utilities.service";
import {DataTableService} from "../shared/services/data-table.service";
import {FormConfigService} from "../shared/services/form-config.service";
import {Subscription} from 'rxjs';

@Component({
	selector: 'app-home-page',
	templateUrl: './home-page.component.html',
	styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit, OnDestroy {
	private subscription: Subscription = new Subscription();

	ptButtonSchema;
	dataSourceCtrl2;
	ptTableSchema2;
	ptFormSchema2;

	constructor(
		private _bus: ButtonsUtilitiesService,
		private _dts: DataTableService,
		private _fcs: FormConfigService,
	) {
	}

	ngOnInit() {
		console.log('this.constructor.name ' + this.constructor.name);
		// buttons
		this.subscription.add(
			this._bus.getButtonsConfig()
				.pipe(
					elementAt(1)
				).subscribe((res: any) => {
				this.ptButtonSchema = res;
				return this.ptButtonSchema;
			})
		)
		// table
		this.subscription.add(
			this._dts.getDataSourceConfig()
				.pipe(
					elementAt(1)
				).subscribe((res: any) => {
				this.dataSourceCtrl2 = res;
				return this.dataSourceCtrl2;
			})
		)

		this.subscription.add(
			this._dts.getTableSchemaConfig()
				.pipe(
					elementAt(1)
				).subscribe((res: any) => {
				this.ptTableSchema2 = res;
				return this.ptTableSchema2;
			})
		)
		// form
		this.subscription.add(
			this._fcs.getFormConfig()
				.subscribe((res: any) => {
					this.ptFormSchema2 = res;
					return this.ptFormSchema2;
				})
		)
	}

	ngOnDestroy() {
		this.subscription.unsubscribe();
		if (!this.subscription.closed) {
			console.log('NOT SUBSCRIBED!');
		} else {
			console.log('Unsubscripted');
		}
	}
}
